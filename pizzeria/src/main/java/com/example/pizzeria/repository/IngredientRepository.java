package com.example.pizzeria.repository;

import com.example.pizzeria.entity.Ingredient;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 */
@Repository
public interface IngredientRepository extends CrudRepository<Ingredient, Long> {
}
