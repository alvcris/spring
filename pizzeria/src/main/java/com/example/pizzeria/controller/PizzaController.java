package com.example.pizzeria.controller;

import com.example.pizzeria.Service.PizzaService;
import com.example.pizzeria.entity.Pizza;
import com.example.pizzeria.entity.PizzaCategory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 */
@Controller
public class PizzaController {

   @Autowired
   PizzaService pizzaService;

   @GetMapping("/pizzas")
   public ModelAndView pizza(){
      ModelAndView mv = new ModelAndView("/pizza");
      mv.addObject("pizzas", pizzaService.getAllPizzas());
      return mv;
   }

   @PostMapping("/pizza")
   @ResponseBody
   public void addPizza(){
      Pizza pizza = new Pizza();
      pizza.setName("pizza1");
      pizza.setPizzaCategory(PizzaCategory.LARGE);
      pizza.setPrice(100.0);
      pizzaService.addPizza(pizza);
   }
}
