package com.example.pizzeria.controller;

import com.example.pizzeria.Service.IngredientService;
import com.example.pizzeria.entity.Ingredient;
import com.example.pizzeria.entity.IngredientsCategory;
import com.example.pizzeria.exceptions.IngredientException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
//import org.apache.commons.lang.StringUtils;


import javax.validation.Valid;

/**
 */
@Controller
@RequestMapping("/ingredients")
public class IngredientsController {

   private static final int ERROR_FIELD_INDEX_ZERO = 0;

   @Autowired
   private IngredientService ingredientService;

   @GetMapping
   public ModelAndView getAllIngredients(){
      ModelAndView mv = new ModelAndView("/ingredients");
      mv.addObject("ingredients", ingredientService.getAllIngredient());
      mv.addObject("ingredientsCategory", IngredientsCategory.values());
      return mv;
   }

   @PostMapping
   public String saveIngredient(
         @Valid
         @ModelAttribute
         final Ingredient ingredient,
         final BindingResult bindingResult,
         final Model model){
      if(bindingResult.hasErrors()) {
         throw new IngredientException();
      }

      ingredientService.save(ingredient);
      model.addAttribute("ingredients", ingredientService.getAllIngredient());
      model.addAttribute("ingredientsCategory", IngredientsCategory.values());
      return "ingredients-main-table";
   }

   @DeleteMapping(value = "{id}")
   public ResponseEntity<String> deleteIngredient(@PathVariable final Long id){
      try{
         ingredientService.delete(id);
         return new ResponseEntity<String>(HttpStatus.NO_CONTENT);
      }catch (Exception exception){
         return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
      }
   }

   @GetMapping(value = "{id}")
   @ResponseBody
   public Ingredient getIngredient(@PathVariable final Long id){
      return ingredientService.getIngredient(id);
   }

}
