package com.example.pizzeria.config;

import com.example.pizzeria.entity.Pizza;
import com.example.pizzeria.repository.PizzaRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.text.DecimalFormat;
import java.util.Random;
import java.util.stream.IntStream;

/**
 * Configuration class to initialize database
 */
@Configuration
public class ConfigHelper {

    private static final Logger logger = LoggerFactory.getLogger(ConfigHelper.class);
    private static final int START_INCLUSIVE = 1;
    private static final int END_EXCLUSIVE = 20;

    @Autowired
    private PizzaRepository pizzaRepository;

    /**
     * This will initiate database with default values
     */
    @PostConstruct
    public void databaseInit(){
        logger.info("Adding initial pizzas");

        IntStream.range(START_INCLUSIVE, END_EXCLUSIVE).forEach(index -> {
            Pizza pizza = new Pizza();
            pizza.setName(String.format("Pizza %s", index));
            pizza.setPrice(getRandomDouble() + START_INCLUSIVE);
            pizzaRepository.save(pizza);
        });
    }

    private Double getRandomDouble(){
        DecimalFormat df = new DecimalFormat(".##");
        return Double.valueOf(df.format(new Random().nextDouble()));
    }
}
