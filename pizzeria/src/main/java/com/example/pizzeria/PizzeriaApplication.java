package com.example.pizzeria;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan(basePackages = "com.example.pizzeria")
@EntityScan("com.example.pizzeria.entity")
@EnableJpaRepositories("com.example.pizzeria.repository")
public class PizzeriaApplication {

   public static void main(String[] args) {
      SpringApplication.run(PizzeriaApplication.class, args);
   }
}
