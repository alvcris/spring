package com.example.pizzeria.Service;

import com.example.pizzeria.entity.Pizza;
import com.example.pizzeria.repository.PizzaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**

 */
@Component
public class PizzaService {

   @Autowired
   PizzaRepository pizzaRepository;

   public void addPizza(final Pizza pizza){
      pizzaRepository.save(pizza);
   }

   public String count(){
      return String.valueOf(pizzaRepository.count());
   }

   public Iterable<Pizza> getAllPizzas(){
       return pizzaRepository.findAll();
   }
}
