package com.example.pizzeria.Service;

import com.example.pizzeria.entity.Ingredient;
import com.example.pizzeria.repository.IngredientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 */
@Component
public class IngredientService {

   @Autowired
   IngredientRepository ingredientRepository;

   public void addIngredient(final Ingredient ingredient){
      ingredientRepository.save(ingredient);
   }

   public Iterable<Ingredient> getAllIngredient(){
      return ingredientRepository.findAll();
   }

   public void save(final Ingredient ingredient) {
      Ingredient savedIngredient = ingredientRepository.save(ingredient);
   }

   public void delete(final Long id) {
      ingredientRepository.delete(id);
   }

   public Ingredient getIngredient(final Long id) {
      return ingredientRepository.findOne(id);
   }
}
