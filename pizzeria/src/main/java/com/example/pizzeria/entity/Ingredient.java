package com.example.pizzeria.entity;

import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**

 */
@Entity
public class Ingredient {

   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   private Long id;

   @NotBlank
   private String name;

   @Enumerated(EnumType.STRING)
   private IngredientsCategory ingredientsCategory;

   public Long getId() {
      return id;
   }

   public void setId(Long id) {
      this.id = id;
   }

   public String getName() {
      return name;
   }

   public void setName(String name) {
      this.name = name;
   }

   public IngredientsCategory getIngredientsCategory() {
      return ingredientsCategory;
   }

   public void setIngredientsCategory(IngredientsCategory ingredientsCategory) {
      this.ingredientsCategory = ingredientsCategory;
   }

   @Override
   public boolean equals(Object o) {
      if (this == o)
         return true;
      if (o == null || getClass() != o.getClass())
         return false;

      Ingredient that = (Ingredient) o;

      if (name != null ? !name.equals(that.name) : that.name != null)
         return false;
      return ingredientsCategory == that.ingredientsCategory;
   }

   @Override
   public int hashCode() {
      int result = name != null ? name.hashCode() : 0;
      result = 31 * result + (ingredientsCategory != null ? ingredientsCategory.hashCode() : 0);
      return result;
   }
}
