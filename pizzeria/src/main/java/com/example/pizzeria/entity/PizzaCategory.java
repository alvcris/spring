package com.example.pizzeria.entity;

/**

 */
public enum PizzaCategory {
   SMALL, MEDIUM, LARGE
}
