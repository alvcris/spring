package com.example.pizzeria.entity;

/**

 */
public enum IngredientsCategory {
   CHEESE, SALAD, MEAT
}
