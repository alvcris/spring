package com.example.pizzeria.entity;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.util.Set;

/**
 */
@Entity
public class Pizza {

   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   private Long id;

   private String name;

   private Double price;

   @Enumerated(EnumType.STRING)
   private PizzaCategory pizzaCategory;

   @ManyToMany
   private Set<Ingredient> ingredientSet;

   public Long getId() {
      return id;
   }

   public String getName() {
      return name;
   }

   public void setName(String name) {
      this.name = name;
   }

   public Double getPrice() {
      return price;
   }

   public void setPrice(Double price) {
      this.price = price;
   }

   public PizzaCategory getPizzaCategory() {
      return pizzaCategory;
   }

   public void setPizzaCategory(PizzaCategory pizzaCategory) {
      this.pizzaCategory = pizzaCategory;
   }

   public Set<Ingredient> getIngredientSet() {
      return ingredientSet;
   }

   public void setIngredientSet(Set<Ingredient> ingredientSet) {
      this.ingredientSet = ingredientSet;
   }

   @Override
   public boolean equals(Object o) {
      if (this == o)
         return true;
      if (o == null || getClass() != o.getClass())
         return false;

      Pizza pizza = (Pizza) o;

      if (name != null ? !name.equals(pizza.name) : pizza.name != null)
         return false;
      if (price != null ? !price.equals(pizza.price) : pizza.price != null)
         return false;
      if (pizzaCategory != pizza.pizzaCategory)
         return false;
      return ingredientSet != null ? ingredientSet.equals(pizza.ingredientSet) : pizza.ingredientSet == null;
   }

   @Override
   public int hashCode() {
      int result = price != null ? price.hashCode() : 0;
      result = 31 * result + (pizzaCategory != null ? pizzaCategory.hashCode() : 0);
      result = 31 * result + (ingredientSet != null ? ingredientSet.hashCode() : 0);
      return result;
   }
}
