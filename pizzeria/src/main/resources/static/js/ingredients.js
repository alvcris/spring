$(document).ready(function(){
    //adding listeners
    listeners();
});

var updateComponentAndListeners = function(componentToUpdate, page){
    $(componentToUpdate).html(page);
    listeners();
}

var hide = function(){
    $('#modal-add-ingredient').modal('hide');
}

var listeners = function(){
    resetModal();
    $('.btn-delete').on('click', function(){
        var id = $(this).parents('tr').data('id');
        var url = 'ingredients/' + id;

        $.ajax({
            url: url,
            type: 'DELETE',
            success: function(result) {
                $('tr[data-id = "'+id+'"]').remove();
            }
        });
    });

    $('.btn-edit').on('click', function(){
            var id = $(this).parents('tr').data('id');
            var url = 'ingredients/' + id;
            doGet(url);
        });

    $('#btn-save').on('click', function(){
        var url = 'ingredients';
        var ingredient = $('#form-ingredient').serialize();
        doPost(url, ingredient, '#section-ingredients');
    });
}

var doPost = function(url, ingredient, componentToUpdate) {
$.post(url, ingredient)
    .done(function(page){
        updateComponentAndListeners(componentToUpdate, page);
    })
    .fail(function(){
        alert('error')
    })
    .always(function(){
        //need to solve problem to hide modal and remove page reload
        window.location.reload();
        //hide();
    });
}

var doGet = function(url){
    $.ajax({
        url: url,
        type: 'GET',
        success: function(ingredient){
            updateModal(ingredient);
        }
    });
}

var updateModal = function(ingredient){
    $('#name').val(ingredient.name);
    $('#ingredientsCategory').val(ingredient.ingredientsCategory);
    $('#id').val(ingredient.id)

    $('#modal-add-ingredient').modal('show');
}

var resetModal = function(){
    $('#modal-add-ingredient').on('hide.bs.modal', function(){
        $('#id').val("");
        $('#name').val("");
        $('#ingredientsCategory').val("");
    });
}